
When I type '{{' in vim I get this:
            {
                | # Cursor position 
            }
This behavior is exactly what I want.


However, when I copied the same plugin I use in vim under neovim, typing '{{' 
gives me this:
            {
            | # Cursor position
            }
...with the cursor not indented.  


I directly copied the autoclose.vim file installed under vim as a neovim 
plugin and I'm getting the indent in vim but not in neovim.

So the indentation I'm seeing in vim is /not/ coming from the autoclose addon 
itself; some other vim setting or plugin is providing the indentation, and I'm 
not sure what that plugin or setting is.

I have edited the plugin's code (see the JDB comment around line 124) to add 
the tab where I want it, so this plugin does behave in neovim the way I want 
it to now, though I'm still a bit mystified as to why it works at all in vim.

I mean, there's obviously some .rc file setting or other addon in vim that's 
giving me that indent, I just don't know exactly what it is.



History
    I've used this addon forever.  It was created in 2007 and last touched in 
    2009, and it looks like the 2009 just increased backcompat so the addon 
    would work for vim 6.0 when it had originally been written for vim 7.0.

    The point is that the addon's creation predates Github and so the addon 
    never existed there, only on vim.org.  I /have/ found it on github, but it 
    appears to just be that somebody, not the original author, copied the 
    original from vim.org and slapped it up on github.

    Vim.org - https://www.vim.org/scripts/script.php?script_id=1849
    Github copy - https://github.com/vim-scripts/AutoClose
    My Gitlab "fork" and update - https://gitlab.com/jdb-public-tools/vim-plugins/autoclose

